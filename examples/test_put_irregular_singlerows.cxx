#include <upcxx.h>
#include <upcxx/array.h>
#include <darray.h>
#include <iostream>
using namespace std;
using namespace upcxx;

int main(int argc, char** argv) {

  /* Initialize UPC++ */
  init(&argc, &argv);

  const int ndims = 2;
  const int nrows = 8;
  const int ncols = 1;
  // TODO: const int nelems = 54;
  const int nelems = 33;

  int block_rows[nrows+1] = {0, 1, 2, 3, 4, 5, 6, 7, 8};
  int block_cols[ncols+1] = {0, 33};

  ndarray<double, ndims, local> myblock;
  rectdomain<ndims> global_rd, local_rd;

  global_rd = RD( PT( 0, 0 ), PT( ranks(), nelems ) );

  local_rd  = RD( PT( myrank(), 0), PT( myrank()+1,  nelems) );

  myblock.create( local_rd );

  upcxx_foreach( pt, local_rd ) {
    myblock[pt] = pt.x[0] + pt.x[1];
  };

  DArray<double, ndims> D(nrows, ncols, global_rd, myblock);


  //if (myrank() == 2) {
  //  cout << "elements per row: " << D.get_elems_per_row() << endl;
  //  cout << "elements per col: " << D.get_elems_per_col() << endl;
  //  cout << "my row, col = " << D.get_myrow() << ", " << D.get_mycol() << endl;
  //  cout << "[low,high] = [" << D.get_lowpt() << ", " << D.get_highpt() << "]\n";

  //  cout << "my lowpt data = " << D.get_local_block()[D.get_lowpt()] << endl;
  //  cout << "my highpt data = " << D.get_local_block()[D.get_highpt()] << endl;


  //  ndarray<double, ndims, local> tile;
  //  point<2> upper_left  = PT(1,10); 
  //  point<2> lower_right = PT(2,15);
  //  rectdomain<2> rd = RD( upper_left, lower_right );

  //  tile.create( rd );
  //  upcxx_foreach( pt, global_rd ) {
  //    tile[pt] = pt.x[0] + pt.x[1];
  //  };

  //   D.put_tile( tile );

  //}

  /* Assure tile has been written to the darray */
  barrier();

  if (myrank() == 3) {

    ndarray<double, ndims> tile;

    point<2> upper_left  = PT(1,0);
    point<2> lower_right = PT(2,20);

    rectdomain<2> rd = RD( upper_left, lower_right );

    //tile = D.get_tile_fg( upper_left, lower_right );
    //tile.create( rd ); D.get_tile( &tile );
    tile = D( upper_left, lower_right );

    cout << "delems:";
    upcxx_foreach(pt, rd ) {
      if (( pt.x[1]-(upper_left.x[1]) ) % \
          (lower_right.x[1] - upper_left.x[1]) == 0) cout << endl;
      cout << tile[pt] << ", ";
    };
    cout << endl;


  }

  barrier();

  //D.print();
  D.print(false);

  /* Finalize UPC++ */
  finalize();

  return 0;

}
