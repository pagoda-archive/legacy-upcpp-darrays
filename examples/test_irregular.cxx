#include <upcxx.h>
#include <upcxx/array.h>
#include <darray.h>
#include <darray.cxx>
#include <iostream>
using namespace std;
using namespace upcxx;

int main(int argc, char** argv) {

  /* Initialize UPC++ */
  init(&argc, &argv);

  const int ndims = 2;
  const int nrows = 2;
  const int ncols = 3;
  // TODO: const int nelems = 54;
  const int nelems = 30;


  int block_rows[nrows+1] = {0, 5, 30};
  int block_cols[ncols+1] = {0, 10, 15, 30};

  ndarray<double, ndims, local> myblock;
  rectdomain<ndims> global_rd, local_rd;

  global_rd = RD( PT( block_rows[0], block_cols[0] ),\
                  PT( block_rows[nrows], block_cols[ncols] ) );

  point<2> up_left =   PT( block_rows[  myrank()/ncols  ], \
                           block_cols[  myrank()%ncols  ] );
  point<2> low_right = PT( block_rows[ myrank()/ncols+1 ], \
                           block_cols[ myrank()%ncols+1 ] );

  local_rd  = RD( up_left, low_right );

  //sleep(myrank());
  //cout << "\nrank: " << myrank() << endl;
  //cout << "lo: " << myrank()/ncols << ", " << myrank()%ncols << endl;
  //cout << "hi: " << myrank()/ncols+1 << ", " << myrank()%ncols+1 << endl;

  myblock.create( local_rd );

  //upcxx_foreach( pt, local_rd ) {
  //  if (( pt.x[1]-(up_left.x[1]) ) % \
  //     (low_right.x[1] - up_left.x[1]) == 0) cout << endl;
  //     cout << pt << ", ";
  //}; cout << endl;

  DArray<double, ndims> D(nrows, ncols, global_rd, myblock);


  if (myrank() == 2) {
    cout << "(average) elements per row: " << D.get_elems_per_row() << endl;
    cout << "(average) elements per col: " << D.get_elems_per_col() << endl;
    cout << "my row, col = " << D.get_myrow() << ", " << D.get_mycol() << endl;
    cout << "[low,high] = [" << D.get_lowpt() << ", " << D.get_highpt() << "]\n";

    cout << "my lowpt data = " << D.get_local_block()[D.get_lowpt()] << endl;
    cout << "my highpt data = " << D.get_local_block()[D.get_highpt()] << endl;

    ndarray<double, ndims, local> tile;


    /* Tiled get tests: */
    //point<2> upper_left  = PT(6,11);   // 4,
    //point<2> lower_right = PT(8,13);

    //point<2> upper_left  = PT(1,1);      // 0, 
    //point<2> lower_right = PT(25,2);     // 3,

    //point<2> upper_left  = PT(0,20);       // 2,
    //point<2> lower_right = PT(21,26);      // 5,

    //point<2> upper_left  = PT(4,14);       // 1, 2,
    //point<2> lower_right = PT(6,16);      // 4, 5,
    point<2> upper_left  = PT(1,1);       // 1, 2,
    point<2> lower_right = PT(27,27);      // 4, 5,

    //tile = D.get_tile_fg( upper_left, lower_right );
    tile = D( upper_left, lower_right );

    rectdomain<2> rd = RD( upper_left, lower_right );

    cout << "delems:";
    upcxx_foreach(pt, rd ) {
      if (( pt.x[1]-(upper_left.x[1]) ) % \
          (lower_right.x[1] - upper_left.x[1]) == 0) cout << endl;
      cout << tile[pt] << ", ";
    };
    cout << endl;

  }

  barrier();

  /* Finalize UPC++ */
  finalize();

  return 0;

}
