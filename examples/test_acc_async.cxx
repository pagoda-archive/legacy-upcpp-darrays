#include <upcxx.h>
#include <upcxx/array.h>
#include <darray.h>
#include <iostream>
using namespace std;
using namespace upcxx;


void my_func( ndarray<double,2,global> local_block, \
              ndarray<double,2,global> remote_block ) {

  cout << "rank " << myrank() << " has data from: " \
       <<  remote_block.domain() << endl;

  ndarray<double,2,global> block2(remote_block.domain());
  block2.copy(remote_block);

  upcxx_foreach( pt, block2.domain() ) {
    local_block[pt] += block2[pt];
  };

  return;
}


int main(int argc, char** argv) {

  init(&argc, &argv);

  const int ndims = 2;
  const int nrows = 2;
  const int ncols = 3;
  const int nelems = 30;

  int block_rows[nrows+1] = {0, 5, 30};
  int block_cols[ncols+1] = {0, 10, 15, 30};

  ndarray<double, ndims, local> myblock;
  rectdomain<ndims> global_rd, local_rd;

  global_rd = RD( PT( block_rows[0], block_cols[0] ),\
                  PT( block_rows[nrows], block_cols[ncols] ) );

  local_rd  = RD( PT( block_rows[  myrank()/ncols  ],   \
                      block_cols[  myrank()%ncols  ] ), \
                  PT( block_rows[ myrank()/ncols+1 ],   \
                      block_cols[ myrank()%ncols+1 ] ) );

  myblock.create( local_rd );

  DArray<double, ndims> D(nrows, ncols, global_rd, myblock);
  D.fill(0.0);

  if (myrank() == 0) {

    double *A = (double *)_mm_malloc(15*15* sizeof (double), 64);
    for (int i=0; i<15; i++) {
      for (int j=0; j<15; j++) {
        A[i*15+j] = 2;
      }
    }

    point<2> upper_left  = PT(4,4); 
    point<2> lower_right = PT(14,14);
    D.put_storage( upper_left, lower_right, A );


    upper_left  = PT(3,3); 
    lower_right = PT(15,15);
    int ld = 15;
    D.acc_async_ld( upper_left, lower_right, A, ld, my_func );
    //async(1)( my_func, D, myblock );
    async_wait();

  }
  barrier();

  if (myrank() == 3) {

    ndarray<double, ndims> tile;
    double *Z = (double *)_mm_malloc(20*20* sizeof (double), 64);

    point<2> upper_left  = PT(0,0);
    point<2> lower_right = PT(20,20);

    //tile = D.get_tile_fg( upper_left, lower_right );
    //tile.create( rd ); D.get_tile( &tile );
    //tile = D( upper_left, lower_right );
    D.get_storage( upper_left, lower_right, Z );

    //cout << "delems:";
    //upcxx_foreach(pt, rd ) {
    //  if (( pt.x[1]-(upper_left.x[1]) ) % \
    //      (lower_right.x[1] - upper_left.x[1]) == 0) cout << endl;
    //  cout << tile[pt] << ", ";
    //};
    //cout << endl;
    
    cout << "delems:" << endl;
    for (int i = 0; i<20; i++) {
      for (int j = 0; j<20; j++) {
        cout << Z[i*20 + j] << ", ";
      }cout << endl;
    }  

  }

  barrier();

  finalize();
  return 0;
}
