UPC++ Distributed Arrays Library

To build and run a small test, do something like the following:

`$ autoreconf -if` 

`$ mkdir build; cd build` 

`$ ../configure CC=clang CXX=upc++`  


To run the example, please use 12 processes.

`$ mpirun -np 12 ./examples/test`
