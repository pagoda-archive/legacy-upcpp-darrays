#include "add.h"
using namespace upcxx;
using namespace std;


void DArray_add_matching( double const &alpha, DArray<double,2> &A, \
                          double const &beta, DArray<double,2> &B, \
                          DArray<double,2> &C ) {
#ifdef DEBUG
  if (myrank() == 0) cout << "A:" << endl; 
  A.print();
  if (myrank() == 0) cout << "B:" << endl; 
  B.print();
  if (myrank() == 0) cout << "C:" << endl; 
  C.print();
#endif

  ndarray<double,2> myA_block = A.get_local_block();
  ndarray<double,2> myB_block = B.get_local_block();
  ndarray<double,2> myC_block = C.get_local_block();

#ifdef DEBUG
  sleep(myrank());
  cout << "rank: " << myrank() << endl;
#endif
  upcxx_foreach( pt, myA_block.domain() ) {
#ifdef DEBUG
    cout << "myA[" << pt << "] = " << myA_block[pt] << endl;
#endif
    myC_block[pt] = alpha * myA_block[pt] + beta * myB_block[pt];
  };

  C.update();

  barrier();

  return;
}
