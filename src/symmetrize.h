#ifndef __SYMMETRIZE_H__
#define __SYMMETRIZE_H__

#include "darray.h"

void DArray_symmetrize( DArray<double, 2> & );

#endif
