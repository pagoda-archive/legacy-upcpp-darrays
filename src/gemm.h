#ifndef __GEMM_H__
#define __GEMM_H__

#include "darray.h"

void DArray_DGEMM( char, char, int, int, int, double, \
                   DArray<double,2> &, DArray<double,2> &, double, \
                   DArray<double,2> &);

extern "C" {
  void dgemm_(char*, char*, int*, int*, int*, double*, double*, int*, double*, int*, double*, double*, int*); 
}

#endif
